<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module10.php
 */

/** 
* 10. Создайте функцию, которая принимает слово на английском языке и проверяет, во 
* множественном ли числе находится слово. Проверяйте самый простой вариант.
*/
$string = "bytes";

// 10.1. Function declaration
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $string string
*
* @return string
*/
function checkPlural(string $string):string
{
    if (substr($string, -1) == "s") {
        var_dump(boolval($string));
    } else {
        var_dump(boolval($string));
    }
}

echo checkPlural($string);

// 10.2. Function expression
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $string string
*
* @return string
*/
$checkPlural = function (string $string):string 
{
    if (substr($string, -1) == "s") {
        var_dump(boolval($string));
    } else {
        var_dump(boolval($string));
    }
};

echo $checkPlural($string);
?>