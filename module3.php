<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module3.php
 */

/** 
* 3. Сэму и Фродо надо держаться вместе. Проверьте, нет ли между ними других 
* персонажей.
*/

// 3.1
$arr1 = ["Sam","Frodo","Troll","Balrog","Human"];
$arr1ToString = implode($arr1);

// 3.1.1. Function declaration
/**
* Проверяет, нет ли между значениями других значений.
*
* @param $arr1ToString string
*
* @return string
*/
function checkStringBetweenStrings(string $arr1ToString):string
{
if ($arr1ToString = strstr($arr1ToString, "Sam", true)) {
	return 'false';
}
elseif ($arr1ToString = strstr($arr1ToString, "Frodo", true)) {
	return 'false';
}
else {
	return 'true';
}
}
echo checkStringBetweenStrings($arr1ToString);

// 3.1.2. Function expression
/**
* Проверяет, нет ли между значениями других значений.
*
* @param $arr1ToString string
*
* @return string
*/
$checkStringBetweenStrings = function (string $arr1ToString):string
{
if ($arr1ToString = strstr($arr1ToString, "Sam", true)) {
	return 'false';
}
elseif ($arr1ToString = strstr($arr1ToString, "Frodo", true)) {
	return 'false';
}
else {
	return 'true';
}
};
echo $checkStringBetweenStrings($arr1ToString);

// 3.2.
$arr2 = ["Orc","Frodo","Treant","Saruman","Sam"];
$arr2ToString = implode($arr2);

// 3.2.1. Function declaration
/**
* Проверяет, нет ли между значениями других значений.
*
* @param $arr2ToString string
*
* @return string
*/
function checkStringBetweenStrings(string $arr2ToString):string
{
if ($arr2ToString = strstr($arr2ToString, "Frodo")) {
	return 'false';
}
elseif ($arr2ToString = strstr($arr2ToString, "Sam", true)) {
	return 'false';
}
else {
	return 'true';
}
}
echo checkStringBetweenStrings($arr2ToString);

// 3.2.2. Function expression
/**
* Проверяет, нет ли между значениями других значений.
*
* @param $arr2ToString string
*
* @return string
*/
$checkStringBetweenStrings = function (string $arr2ToString):string
{
if ($arr2ToString = strstr($arr2ToString, "Frodo")) {
	return 'false';
}
elseif ($arr2ToString = strstr($arr2ToString, "Sam", true)) {
	return 'false';
}
else {
	return 'true';
}
};
echo $checkStringBetweenStrings($arr2ToString);

// 3.3.
$arr3 = ["Orc","Sam","Frodo","Gandalf","Legolas"];
$arr3ToString = implode($arr3);

// 3.3.1. Function declaration
/**
* Проверяет, нет ли между значениями других значений.
*
* @param $arr3ToString string
*
* @return string
*/
function checkStringBetweenStrings($arr3ToString)
{
if ($arr3ToString = strstr($arr3ToString, "Sam")) {
	return 'true';
}
elseif ($arr3ToString = strstr($arr3ToString, "Frodo", true)) {
	return 'true';
}
else {
	return 'false';
}
}
echo checkStringBetweenStrings($arr3ToString);

// 3.3.2. Function expression
/**
* Проверяет, нет ли между значениями других значений.
*
* @param $arr3ToString string
*
* @return string
*/
$checkStringBetweenStrings = function ($arr3ToString)
{
if ($arr3ToString = strstr($arr3ToString, "Sam")) {
	return 'true';
}
elseif ($arr3ToString = strstr($arr3ToString, "Frodo", true)) {
	return 'true';
}
else {
	return 'false';
}
};
echo $checkStringBetweenStrings($arr3ToString);
?>