<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module4.php
 */
 
// 4. Найдите второе наибольшее число в массиве.

//4.1.
$arr1 = [1, 2, 3];

// 4.1.1. Function declaration
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr1 array
*
* @return array
*/
function max2Arr(array $arr1):array
{
    sort($arr1);
    $key = count($arr1) - 2;
    return $arr1[$key];
}

echo max2Arr($arr1);;

// 4.1.2. Function expression
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr1 array
*
* @return array
*/
$max2Arr = function (array $arr1):array 
{
    sort($arr1);
    $key = count($arr1) - 2;
    return $arr1[$key];
};
echo $max2Arr ($arr1);

// 4.2
$arr2 = [1, -2, 3];

// 4.2.1. Function declaration
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr2 array
*
* @return array
*/
function max2Arr(array $arr2):array
{
    sort($arr2);
    $key = count($arr2) - 2;
    return $arr2[$key];
}

echo max2Arr($arr2);;

// 4.2.2. Function expression
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr2 array
*
* @return array
*/
$max2Arr = function (array $arr2):array {
    sort($arr2);
    $key = count($arr2) - 2;
    return $arr2[$key];
};
echo $max2Arr ($arr2);

// 4.3
$arr3 = [0, 0, 10];

// 4.3.1. Function declaration
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr3 array
*
* @return array
*/
function max2Arr(array $arr3):array
{
    sort($arr3);
    $key = count($arr3) - 2;
    return $arr3[$key];
}

echo max2Arr($arr3);;

// 4.3.2. Function expression
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr3 array
*
* @return array
*/
$max2Arr = function (array $arr3):array 
{
    sort($arr3);
    $key = count($arr3) - 2;
    return $arr3[$key];
};
echo $max2Arr ($arr3);

// 4.4
$arr4 = [-1, -2, -3];

// 4.4.1. Function declaration
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr4 array
*
* @return array
*/
function max2Arr(array $arr4):array
{
    sort($arr4);
    $key = count($arr4) - 2;
    return $arr4[$key];
}

echo max2Arr($arr4);

// 4.4.2. Function expression
/**
* Находит второе наибольшее число в массиве.
*
* @param $arr4 array
*
* @return array
*/
$max2Arr = function (array $arr4):array 
{
    sort($arr3);
    $key = count($arr4) - 2;
    return $arr4[$key];
};
echo $max2Arr ($arr4);
?>