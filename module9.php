<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module9.php
 */
 
// 9. Создайте функцию, которая трансформирует массив слов в массив длин этих слов.
//9.1. 
$arr1 = ["hello","world"];

// 9.1.1. Function declaration
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $arr1 array
*
* @return array
*/
function calcCharactersToCharacterCount(array $arr1):array
{
foreach ($arr1 as &$el) 
{
    if (is_string($el)) 
	{
        $el = strlen($el);
	}
}
print_r($arr1);
}
echo calcCharactersToCharacterCount($arr1);

// 9.1.2. Function expression
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $arr1 array
*
* @return array
*/
$calcCharactersToCharacterCount = function(array $arr1):array
{
foreach ($arr1 as &$el) 
{
    if (is_string($el)) 
	{
        $el = strlen($el);
	}
}
print_r($arr1);
};
echo $calcCharactersToCharacterCount($arr1);

//9.2. 
$arr2 = ["some","test","data","strings"];

// 9.2.1. Function declaration
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $arr2 array
*
* @return array
*/
function calcCharactersToCharacterCount(array $arr2):array
{
foreach ($arr2 as &$el) 
{
    if (is_string($el)) 
	{
        $el = strlen($el);
	}
}
print_r($arr2);
}
echo calcCharactersToCharacterCount($arr2);

// 9.2.2. Function expression
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $arr2 array
*
* @return array
*/
$calcCharactersToCharacterCount = function(array $arr2):array
{
foreach ($arr2 as &$el) 
{
    if (is_string($el)) 
	{
        $el = strlen($el);
	}
}
print_r($arr2);
};
echo $calcCharactersToCharacterCount($arr2);

//9.3. 
$arr3 = ["clojure"];

// 9.3.1. Function declaration
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $arr3 array
*
* @return array
*/
function calcCharactersToCharacterCount(array $arr3):array
{
foreach ($arr3 as &$el) 
{
    if (is_string($el)) 
	{
        $el = strlen($el);
	}
}
print_r($arr3);
}
echo calcCharactersToCharacterCount($arr3);

// 9.3.2. Function expression
/**
* Трансформирует массив слов в массив длин этих слов.
*
* @param $arr3 array
*
* @return array
*/
$calcCharactersToCharacterCount = function(array $arr3):array
{
foreach ($arr3 as &$el) 
{
    if (is_string($el)) 
	{
        $el = strlen($el);
	}
}
print_r($arr3);
};
echo $calcCharactersToCharacterCount($arr3);
?>