<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module5.php
 */

/** 
* 5. Дан массив строк, создайте функцию, которая создаёт новый массив, содержащий 
* строки, длины которых соответствуют наидлиннейшей строке.
*/

// 5.1.
$arr1 = ["in", "Soviet", "Russia", "frontend", "programms", "you"];

// 5.1.1. Function declaration
/**
* Создаёт новый массив, содержащий строки, длины которых соответствуют наидлиннейшей 
* строке.
*
* @param $arr1 array
*
* @return array
*/
function createArrLongestString(array $arr1):array
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr1 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
}

print_r(createArrLongestString($arr1));

// 5.1.2. Function expression
/**
* Создаёт новый массив, содержащий строки, длины которых соответствуют наидлиннейшей 
* строке.
*
* @param $arr1 array
*
* @return array
*/
$createArrLongestString = function (array $arr1):array 
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr1 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
};

print_r($createArrLongestString($arr1));

// 5.2.1. Function declaration
/**
* Создаёт новый массив, содержащий строки, длины которых соответствуют наидлиннейшей 
* строке.
*
* @param $arr2 array
*
* @return array
*/
function createArrLongestString(array $arr2):array
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr2 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
}

print_r(createArrLongestString($arr2));

// 5.2.2. Function expression
/**
* Создаёт новый массив, содержащий строки, длины которых соответствуют наидлиннейшей 
* строке.
*
* @param $arr2 array
*
* @return array
*/
$createArrLongestString = function (array $arr2):array 
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr2 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
};

print_r($createArrLongestString($arr2));

// 5.3
$arr3 = ["a","b","c","d"];

// 5.3.1. Function declaration
/**
* Создаёт новый массив, содержащий строки, длины которых соответствуют наидлиннейшей 
* строке.
*
* @param $arr3 array
*
* @return array
*/
function createArrLongestString(array $arr3):array
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr3 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
}

print_r(createArrLongestString($arr3));

// 5.3.2. Function expression
/**
* Создаёт новый массив, содержащий строки, длины которых соответствуют наидлиннейшей 
* строке.
*
* @param $arr3 array
*
* @return array
*/
$createArrLongestString = function (array $arr3):array 
{
    $cachedLength = 0;
    $longest = [];
    foreach ($arr3 as $value) {
        $currentLength = strlen($value);
        if ($currentLength > $cachedLength) {
            $longest = [$value];
            $cachedLength = $currentLength;
        } elseif ($currentLength == $cachedLength) {
            $longest[] = $value;
        }
    }
    var_export($longest);
};

print_r($createArrLongestString($arr3));
?>