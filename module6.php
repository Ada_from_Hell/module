<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module6.php
 */

/** 
* 6. Рассчитайте финальную оценку студента по пяти предметам.
* Если средняя оценка больше 90, то итоговая A.
* Если средняя оценка больше 80, то итоговая B.
* Если средняя оценка больше 70, то итоговая оценка C.
* Если средняя оценка больше 60, то итоговая оценка D.
* В остальных случаях итоговая оценка F.
*/
$i = 91;

// 6.1. Function declaration
/**
* Рассчитывает финальную оценку студента по пяти предметам.
*
* @param $i integer
*
* @return integer
*/
function calcGrade(integer $i):integer
{
    if ($i > 90 && $i <= 100) {
        return 'Grade: A';
    } elseif ($i > 80 && $i <= 90) {
        return 'Grade: B';
    } elseif ($i > 70 && $i <= 80) {
        return 'Grade: B';
    } elseif ($i > 60 && $i <= 70) {
        return 'Grade: B';
    } elseif ($i < 60) {
        return 'Grade: F';
    }
}

echo calcGrade($i);

// 6.2. Function expression
/**
* Рассчитывает финальную оценку студента по пяти предметам.
*
* @param $i integer
*
* @return integer
*/
$calcGrade = function (integer $i):integer 
{
    if ($i > 90 && $i <= 100) {
        return 'Grade: A';
    } elseif ($i > 80 && $i <= 90) {
        return 'Grade: B';
    } elseif ($i > 70 && $i <= 80) {
        return 'Grade: B';
    } elseif ($i > 60 && $i <= 70) {
        return 'Grade: B';
    } elseif ($i < 60) {
        return 'Grade: F';
    }
};
echo $calcGrade($i);
?>