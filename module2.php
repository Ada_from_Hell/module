<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module2.php
 */
 
/** 
* 2. Создайте функцию, которая принимает массив из трёх элементов, представляющих 
* собой результат запуска слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*/

// 2.1.
$arr1 = ["9919", "9919", "9919"];

// 2.1.1. Function declaration
/**
* Принимает массив из трёх элементов, представляющих собой результат запуска
* слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*
* @param $arr1 array
*
* @return var_dump
*/
function checkArray(array $arr1):var_dump
{
    $result = "9919";
    foreach ($arr1 as $v) 
	{
        if ($arr1[0] === $result && $arr1[1] === $result && $arr1[2] === $result) 
		{
            $result = true;
        } else 
		{
            $result = false;
        }
        return var_dump($result);
    }
};
echo checkArray($arr1);

// 2.1.2. Function expression
/**
* Принимает массив из трёх элементов, представляющих собой результат запуска
* слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*
* @param $arr1 array
*
* @return var_dump
*/
$checkArray = function (array $arr1):var_dump {
    $result = "9919";
	foreach ($arr1 as $v) 
	{
        if ($arr1[0] === $result && $arr1[1] === $result && $arr1[2] === $result) 
		{
            $result = true;
        } else 
		{
            $result = false;
        }
        return var_dump($result);
    }
};
echo $checkArray($arr1);

// 2.2.
$arr2 = ["abc", "abc", "abb"];

// 2.2.1. Function declaration
/**
* Принимает массив из трёх элементов, представляющих собой результат запуска
* слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*
* @param $arr2 array
*
* @return var_dump
*/
function checkArray(array $arr2):var_dump
{
    $result = "abc";
    foreach ($arr2 as $v) 
	{
        if ($arr2[0] === $result && $arr2[1] === $result && $arr2[2] === $result) 
		{
            $result = true;
        } else 
		{
            $result = false;
        }
        return var_dump($result);
    }
};
echo checkArray($arr2);

// 2.2.2. Function expression
/**
* Принимает массив из трёх элементов, представляющих собой результат запуска
* слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*
* @param $arr2 array
*
* @return var_dump
*/
$checkArray = function(array $arr2:var_dump 
{
    $result = "abc";
	foreach ($arr2 as $v) 
	{
        if ($arr2[0] === $result && $arr2[1] === $result && $arr2[2] === $result) 
		{
            $result = true;
        } else 
		{
            $result = false;
        }
        return var_dump($result);
    }
};
echo $checkArray($arr2);

// 2.3.
$arr3 = ["@","@","@"];

// 2.3.1. Function declaration
/**
* Принимает массив из трёх элементов, представляющих собой результат запуска
* слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*
* @param $arr3 array
*
* @return var_dump
*/
function checkArray(array $arr3):var_dump
{
    $result = "abc";
    foreach ($arr3 as $v) 
	{
        if ($arr3[0] === $result && $arr3[1] === $result && $arr3[2] === $result) 
		{
            $result = true;
        } else 
		{
            $result = false;
        }
        return var_dump($result);
    }
};
echo checkArray($arr3);

// 2.3.2. Function expression
/**
* Принимает массив из трёх элементов, представляющих собой результат запуска
* слот-машины из казино. 
* Проверьте, является ли комбинация элементов удачной (все элементы равны).
*
* @param $arr3 array
*
* @return var_dump
*/
$checkArray = function($arr3) {
    $result = "abc";
	foreach ($arr3 as $v) 
	{
        if ($arr3[0] === $result && $arr3[1] === $result && $arr3[2] === $result) 
		{
            $result = true;
        } else 
		{
            $result = false;
        }
        return var_dump($result);
    }
};
echo $checkArray($arr3);
?>