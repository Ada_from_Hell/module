<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module7.php
 */
 
/** 
* 7. Создайте функцию, которая принимает целое число и возвращает строку с названием 
* фигуры, состоящий из переданного количество сторон.
*/

// 7.1.
$i = 1;

// 7.1. Function declaration
/**
* Принимает целое число и возвращает строку с названием фигуры, состоящий из 
* переданного количество сторон.
*
* @param $i integer
*
* @return integer
*/
function showFigure(integer $i):integer
{
    if ($i == 1) {
        return 'Circle';
    } elseif ($i == 2) {
        return 'Semi-circle';
    } elseif ($i == 3) {
        return 'Triangle';
    } elseif ($i == 4) {
        return 'Square';
    } elseif ($i == 5) {
        return 'Pentagon';
    } elseif ($i == 6) {
        return 'Hexagon';
    } elseif ($i == 7) {
        return 'Heptagon';
    } elseif ($i == 8) {
        return 'Octagon';
    } elseif ($i == 9) {
        return 'Nonagon';
    } elseif ($i == 10) {
        return 'Decagon';
    }
}

echo showFigure($i);

// 7.2. Function expression
/**
* Принимает целое число и возвращает строку с названием фигуры, состоящий из 
* переданного количество сторон.
*
* @param $i integer
*
* @return integer
*/
$showFigure = function (integer $i):integer 
{
    if ($i == 1) {
        return 'Circle';
    } elseif ($i == 2) {
        return 'Semi-circle';
    } elseif ($i == 3) {
        return 'Triangle';
    } elseif ($i == 4) {
        return 'Square';
    } elseif ($i == 5) {
        return 'Pentagon';
    } elseif ($i == 6) {
        return 'Hexagon';
    } elseif ($i == 7) {
        return 'Heptagon';
    } elseif ($i == 8) {
        return 'Octagon';
    } elseif ($i == 9) {
        return 'Nonagon';
    } elseif ($i == 10) {
        return 'Decagon';
    }
};
echo $showFigure($i);
?>