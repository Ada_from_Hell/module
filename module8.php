<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module8.php
 */
 
/** 
* 8. Фермер просит вас посчитать сколько ног у всех его животных.
* Фермер разводит три вида: курицы = 2 ноги, коровы = 4 ноги, свиньи = 4 ноги.
* Фермер посчитал своих животных и говорит вам, сколько их каждого вида.
* Вы должны написать функцию, которая возвращает общее число ног всех животных.
*/
$hen = 2;
$cow = 3;
$pig = 5;

// 8.1. Function declaration
/**
* Принимает целое число и возвращает строку с названием фигуры, состоящий из 
* переданного количество сторон.
*
* @param $hen integer
* @param $cow integer
* @param $pig integer
*
* @return integer
*/
function calcPaws(integer $hen, integer $cow, integer $pig):integer 
{
	return ($hen * 2) + ($cow * 4) + ($pig * 4);
}
echo calcPaws($hen, $cow, $pig);

// 8.2. Function expression
/**
* Принимает целое число и возвращает строку с названием фигуры, состоящий из 
* переданного количество сторон.
*
* @param $hen integer
* @param $cow integer
* @param $pig integer
*
* @return integer
*/
$calcPaws = function (integer $hen, integer $cow, integer $pig):integer 
{
    return ($hen * 2) + ($cow * 4) + ($pig * 4);
};
echo $calcPaws($hen, $cow, $pig);
?>