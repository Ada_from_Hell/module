<?php
/**
 * Php version 5.6
 *
 * @category Mycategory
 * @package  Mypackage
 * @author   Display Name <adeline@example.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     https://gitlab.com/Ada_from_Hell/homework/-/blob/main/module1.php
 */
 
// 1. Создайте функцию, которая подсчитывает количество директорий в массиве.
// 1.1.
$arr1 = ["C:/Projects/something.txt", "file.exe"];
$arr1ToString = implode($arr1);

// 1.1.1. Function declaration
/**
* Подсчитывает количество директорий в массиве.
*
* @param $arr1ToString string
*
* @return integer
*/
function calcDir(string $arr1ToString):integer
{
	return substr_count($arr1ToString,'/');
}
echo calcDir($arr1ToString);

// 1.1.2. Function expression
/**
* Подсчитывает количество директорий в массиве.
*
* @param $arr1ToString string
*
* @return integer
*/
$calcDir = function(string $arr1ToString):integer
{
   return substr_count($arr1ToString,'/');
};
echo $calcDir($arr1ToString);


// 1.2.
$arr2 = ["brain-games.exe","gendiff.sh","task-manager.rb"];
$arr2ToString = implode($arr2);

// 1.2.1. Function declaration
/**
* Подсчитывает количество директорий в массиве.
*
* @param $arr2ToString string
*
* @return integer
*/
function calcDir(string $arr2ToString):integer
{
	return substr_count($arr2ToString,'/');
}
echo calcDir($arr2ToString);

// 1.2.2. Function expression
/**
* Подсчитывает количество директорий в массиве.
*
* @param $arr2ToString string
*
* @return integer
*/
$calcDir = function(string $arr2ToString):integer
{
   return substr_count($arr2ToString,'/');
};
echo $calcDir($arr2ToString);

// 1.3.
$arr3 = ["C:/Users/JohnDoe/Music/Beethoven_5.mp3","/usr/bin","/var/www/myprojectt"];
$arr3ToString = implode($arr3);

// 1.3.1. Function declaration
/**
* Подсчитывает количество директорий в массиве.
*
* @param $arr3ToString string
*
* @return integer
*/
function calcDir(string $arr3ToString):integer
{
	return substr_count($arr3ToString,'/');
}
echo calcDir($arr3ToString);

// 1.3.2. Function expression
/**
* Подсчитывает количество директорий в массиве.
*
* @param $arr3ToString string
*
* @return integer
*/
$calcDir = function(string $arr3ToString):integer
{
   return substr_count($arr3ToString,'/');
};
echo $calcDir($arr3ToString);
?>